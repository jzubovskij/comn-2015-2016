
/* Jevgenij Zubovskij s1346981 */

import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.Arrays;

class Receiver2a {

	// integer sizes for holders for the received packet's data
	static final int payloadSize = 1024;
	static final int headerSize = 3;
	static final int packetSize = 1027;

	// command line arguments
	static String outputFileName;
	static int listeningPort;
	
	//datagram socket
	static DatagramSocket datagramSocket;

	public static void main(String args[]) throws Exception {
		// obtain the listening port from the command line
		listeningPort = Integer.parseInt(args[0]);
		// obtain the file to write to from the command line
		outputFileName = args[1];

		// open the socket to receive incoming packets
		datagramSocket = new DatagramSocket(listeningPort);
		// holders for the received packet's data
		byte data[];
		byte header[];
		byte packet[] = new byte[packetSize];
		// get the file to write to
		File outputFile = new File(outputFileName);
		// delete file if such already exists
		if (outputFile.exists()) {
			outputFile.delete();
		}

		// the expected sequence number
		int expectedPacketNumber = 0;

		// open stream to write (append) to file
		FileOutputStream output = new FileOutputStream(outputFile.getName(), true);
		// prepare the packet holder
		DatagramPacket receivedPacket = new DatagramPacket(packet, packet.length);
		while (true) {
			// while there are still packets to be received, receive them
			datagramSocket.receive(receivedPacket);
			// obtain the header
			header = Arrays.copyOfRange(receivedPacket.getData(), 0, headerSize);
			// obtain the data
			data = Arrays.copyOfRange(receivedPacket.getData(), headerSize, receivedPacket.getLength());
			// get packet sequence number
			int sequenceNumber = byteArrayToInt(new byte[] { 0, 0, header[1], header[2] });

			// if we have received the expected packet
			if (sequenceNumber == expectedPacketNumber) {
				// write the data bytes to the output file
				output.write(data);
			}

			// send reply requesting the newly (or same) requested packet
			InetAddress senderIP = receivedPacket.getAddress();
			// get the port of the sender
			int senderPort = receivedPacket.getPort();

			// send last correctly received packet number's ack 
			byte[] requestedSequenceNumber = intToByteArray(
					(sequenceNumber == expectedPacketNumber) ? expectedPacketNumber : expectedPacketNumber - 1);
			// only take the LSBs as 16 bytes max sequence number
			byte[] packetRequest = { requestedSequenceNumber[2], requestedSequenceNumber[3] };
			// form the daragram packet to be sent
			DatagramPacket sendingPacket = new DatagramPacket(packetRequest, packetRequest.length, senderIP,
					senderPort);

			// send the packet only if there was a previously received packet (so when expecting the 0th packet, we let if time out)
			if (expectedPacketNumber - 1 >= 0) {
				datagramSocket.send(sendingPacket);
			}

			// if we have received the expected packet
			if (sequenceNumber == expectedPacketNumber) {

				//get the next expected sequence number
				expectedPacketNumber++;
				if (header[0] == 1) {
					// close the socket
					datagramSocket.close();
					// close the file
					output.close();
					// exit the program
					System.exit(0);
				}

			}

		}

	}


	// function that return the integer represented by the byte array
	private static int byteArrayToInt(byte[] bytes) {
		return java.nio.ByteBuffer.wrap(bytes).getInt();
	}

	// function that transfroms an integer into a byte array representing it
	private static byte[] intToByteArray(int integer) {
		return ByteBuffer.allocate(4).putInt(integer).array();
	}
}