
/* Jevgenij Zubovskij s1346981 */

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;

class Sender2a {

	// constants for sizes of the packet
	static final int payloadSize = 1024;
	static final int headerSize = 3;
	static final int packetSize = 1027;

	// command line arguments
	static String inputFileName;
	static int listeningPort;
	static String ipName;
	static int retryTimeout = 20;
	static int windowSize = 50;

	// time counter for the transmission time
	static int transmissionTime = 0;
	// start time
	static int startTime = 0;
	// total resending counter
	static int totalRetransissions = 0;
	// the throughput
	static double throughput = 0;

	// data to be sent
	static byte[] fileData;

	// packet count to be sent
	static int packetCount = 0;

	// maximum number of timeouts
	static int timeoutMax = 100;

	// timeouts when sending last window
	static int timeoutsOnPacket = 0;

	// making the base global
	static int basePacketNumber = 0;

	// timer and task to time out the system
	static TimerTask timeoutTask;
	static Timer timeoutTimer;

	// latch to synchronise the timeout and the datagram socket
	static Object latch;

	// the datagram socket
	static DatagramSocket datagramSocket;

	// threads holding the ack receiver and sender threads
	static Thread sender;
	static Thread receiver;
	
	
	//variable to hold the packets in the current window
	static DatagramPacket[] packetsInWindow;

	// class designed to run as the sending thread
	private static class SendWindowTask implements Runnable {

		@Override
		public void run() {

			// send all the payloads in packets
			while (basePacketNumber < packetCount) {
				// send all packets in the current window
				for (int i = 0; i < windowSize && i + basePacketNumber < packetCount; i++) {

					try {
						datagramSocket.send(packetsInWindow[i]);
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
				//lock the thread until unlocked by a timer or ack receiver
				synchronized (latch) {
					try {
						latch.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}

			}
		}
	}

	// the class designed to run as the ack thread
	static class AckTask implements Runnable {

		@Override
		public void run() {
			//do this until all packets were sent (or terminated via same condition)
			while (basePacketNumber < packetCount) {

				// set the holder for the reply data
				DatagramPacket receivedPacket = getNextRecievalPacket();
				// timeout the waiting time for the reply after the set time
				try {

					// time out the connection if packets not being received
					datagramSocket.setSoTimeout(retryTimeout * timeoutMax);
					try {

						// try receiving the data
						datagramSocket.receive(receivedPacket);
						// then get the data
						byte[] receivedData = receivedPacket.getData();
						// get the sequence number the Receiver ack'ed
						int receivedRequestedSequenceNumber = byteArrayToInt(
								new byte[] { 0, 0, receivedData[0], receivedData[1] });

						// make sure we are up to date with the latest ACK
						if (receivedRequestedSequenceNumber >= basePacketNumber) {

							basePacketNumber = receivedRequestedSequenceNumber + 1;

							//renew the packets in the current window
							for(int i =0; i < windowSize && i + basePacketNumber < packetCount; i ++)
							{
								packetsInWindow[i] = getNextPacket(basePacketNumber + i, packetCount, fileData);
							}
							
							// restart the timer for the window (and unlock thread to send the new window)
							stopTimeout();
							startTimeout();

						}
					} catch (SocketException e) {
						// otherwise system should exit
					}

				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

	// method designated to stop the timeout
	static void stopTimeout() {
		//cancel the timer task
		timeoutTask.cancel();
		// reset the timeout on packet
		timeoutsOnPacket = 0;
		//purge the old task queue
		timeoutTimer.purge();
		//notify the thread it can send the window of packets again
		synchronized (latch) {
			latch.notifyAll();
		}

	}

	// method designated to restart the timeout time
	static void startTimeout() {
		
		timeoutTask = new TimerTask() {

			@Override
			public void run() {
				 //System.out.println("TIMED OUT");
				synchronized (latch) {
					//increase the global and packet timeout counters
					totalRetransissions++;
					timeoutsOnPacket++;
					// if sending last window has timed out, terminate
					if (timeoutsOnPacket == timeoutMax) {
						basePacketNumber = packetCount;
					}
					// unlock the while loop in the sending thread
					latch.notifyAll();
				}
			}
		};
		// will time out the system and unlock the sending thread every retryTimeout
		timeoutTimer.schedule(timeoutTask, retryTimeout, retryTimeout);

	}

	public static void main(String args[]) throws Exception {

		// obtain the ip name to send to
		ipName = args[0];
		// obtain the sending port from the command line
		listeningPort = Integer.parseInt(args[1]);
		// obtain the file to send to from the command line
		inputFileName = args[2];

		// if we have the delay as the argument
		if (args.length > 3) {
			// obtain the timeout value from the command line
			retryTimeout = Integer.parseInt(args[3]);
		}

		// if we have the window size as the argument
		if (args.length > 4) {
			// obtain the window size value from the command line
			windowSize = Integer.parseInt(args[4]);
		}

		// creating the sending socket
		datagramSocket = new DatagramSocket();

		// get the file using the filename
		File file = new File(inputFileName);
		// read all the data from the file
		fileData = new byte[(int) file.length()];
		// open a file stream to read from the file
		DataInputStream dataInputStream = new DataInputStream(new FileInputStream(file));
		// read all the bytes from the file
		dataInputStream.readFully(fileData);
		// close data input stream as all data read
		dataInputStream.close();

		// obtain payload (and hence packet) count to be sent
		packetCount = (int) Math.ceil(fileData.length / (payloadSize * 1.0f));

		// record the transmission satrt time
		startTime = (int) System.currentTimeMillis();
		
		//get the initial packets
		packetsInWindow = new DatagramPacket[windowSize];
		for(int i =0; i < windowSize && i + basePacketNumber < packetCount; i ++)
		{
			packetsInWindow[i] = getNextPacket(basePacketNumber + i, packetCount, fileData);
		}
		// initialise the timer
		timeoutTimer = new Timer();
		
		
		// initialise the latch
		latch = new Object();

		// create and start the sending and ack-receiving threads
		sender = new Thread(new SendWindowTask());
		sender.start();

		receiver = new Thread(new AckTask());
		receiver.start();

		// start timing out the sender
		startTimeout();

		// make sure main does not finish before the threads do
		sender.join();
		//receiver.join();

		// exit the system
		teminateSender();
	}

	// method designated to terminate the sender and print the statistics
	private static void teminateSender() {
		// end of transmission counter here as the remaining actions after the
		// ACK is minimal
		transmissionTime = ((int) System.currentTimeMillis()) - startTime;
		// throughput calculations in KB/s
		throughput = (packetCount * 1000.0f) / (transmissionTime);
		// printing out the results
		System.out.printf(
				"-------------------------------------------\nThroughput: %.3f KB/s \nRetransmission count: %d\n-------------------------------------------\n",
				throughput, totalRetransissions);

		// when done sending close the socket and exit
		datagramSocket.close();

		System.exit(0);

	}

	// function that returns an empty datagram using the defined parameters
	private static DatagramPacket getNextRecievalPacket() {
		return new DatagramPacket(new byte[packetSize], packetSize);
	}

	//method that returns the datagram of the specified number form the file being sent
	public static DatagramPacket getNextPacket(int packetNumber, int packetCount, byte[] fileData)
			throws UnknownHostException {
		// prepare buffers for sending the data
		byte[] data;
		byte[] header = new byte[headerSize];
		byte[] packetContents;
		// get the next payload from the file
		data = Arrays.copyOfRange(fileData, packetNumber * payloadSize,
				((packetNumber + 1) * payloadSize >= fileData.length) ? fileData.length
						: (packetNumber + 1) * payloadSize);
		// set header 0th bit to 1 if this is the lays packet to be sent
		header[0] = (byte) (((packetNumber + 1 == packetCount) ? 1 : 0) & (0xff));
		// transform the packet number into a byte array

		byte sequenceNumber[] = intToByteArray(getNextPacketNumber(packetNumber));

		// using bytes 2 and 3 as we have byte order as such MSB .... LSB and we use the two LSBs
		header[1] = sequenceNumber[2];
		header[2] = sequenceNumber[3];
		// make the packet out of the header and the payload
		packetContents = concatenateByteArrays(header, data);

		// getting IP from the ip name
		InetAddress receiverIP = InetAddress.getByName(ipName);
		// prepare the datagram for sending
		DatagramPacket packet = new DatagramPacket(packetContents, packetContents.length, receiverIP, listeningPort);
		// send the datagram
		return packet;
	}

	//method returning the same number it received as the argument (for extensibility, if ever needed)
	private static int getNextPacketNumber(int packetNumber) {
		return packetNumber;
	}

	// function that concatenates two byte arrays into one in the order they are in the arguments
	private static byte[] concatenateByteArrays(byte[] arrayOne, byte[] arrayTwo) {
		// creates the result array big enough to hold both arguments
		byte[] result = new byte[arrayOne.length + arrayTwo.length];
		System.arraycopy(arrayOne, 0, result, 0, arrayOne.length);
		System.arraycopy(arrayTwo, 0, result, arrayOne.length, arrayTwo.length);
		// returns the concatenation result
		return result;
	}

	// function that transforms an integer into a byte array representing it
	private static byte[] intToByteArray(int integer) {
		return ByteBuffer.allocate(4).putInt(integer).array();
	}

	// function that return the integer represented by the byte array
	private static int byteArrayToInt(byte[] bytes) {
		return java.nio.ByteBuffer.wrap(bytes).getInt();
	}

}