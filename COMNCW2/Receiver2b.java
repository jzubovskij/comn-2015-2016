
/* Jevgenij Zubovskij s1346981 */

import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.Arrays;

class Receiver2b {

	// integer sizes for holders for the received packet's data
	static final int payloadSize = 1024;
	static final int headerSize = 3;
	static final int packetSize = 1027;

	// command line arguments
	static String outputFileName;
	static int listeningPort;
	static int windowSize = 10;

	
	//the data from the packet in the current window
	static byte[][] windowData;

	//array holding indication of which packets were received
	static boolean[] receivedInWindow;

	// the expected sequence number
	static int baseWindowPacketNumber = 0;

	//the socket for sending and receiving
	static DatagramSocket datagramSocket;

	public static void main(String args[]) throws Exception {
		// obtain the listening port from the command line
		listeningPort = Integer.parseInt(args[0]);
		// obtain the file to write to from the command line
		outputFileName = args[1];

		//obtain the window size from the command line
		if (args.length > 2) {
			windowSize = Integer.parseInt(args[2]);
		}

		//initialise the array to hold the data in the current window
		windowData = new byte[windowSize][payloadSize];
		//initialise the array to hold the confirmation of receiving packages for the current window
		receivedInWindow = new boolean[windowSize];
		//make all values in said array to be false as nothing yet was received
		Arrays.fill(receivedInWindow, false);

		// open the socket to receive incoming packets
		datagramSocket = new DatagramSocket(listeningPort);
		// holders for the received packet's data
		byte data[];
		byte header[];
		byte packet[] = new byte[packetSize];
		// get the file to write to
		File outputFile = new File(outputFileName);
		// delete file if such already exists
		if (outputFile.exists()) {
			outputFile.delete();
		}

		// open stream to write (append) to file
		FileOutputStream output = new FileOutputStream(outputFile.getName(), true);
		// prepare the packet holder
		DatagramPacket receivedPacket = new DatagramPacket(packet, packet.length);
		while (true) {
			// while there are still packets to be received, receive them
			datagramSocket.receive(receivedPacket);
			// obtain the header
			header = Arrays.copyOfRange(receivedPacket.getData(), 0, headerSize);
			// obtain the data
			data = Arrays.copyOfRange(receivedPacket.getData(), headerSize, receivedPacket.getLength());
			// get packet sequence number
			int sequenceNumber = byteArrayToInt(new byte[] { 0, 0, header[1], header[2] });

			//if it is a packet from the previous window
			if (sequenceNumber < baseWindowPacketNumber) {
				//confirm it has already been received
				sendAck(receivedPacket, sequenceNumber);
				//if it is a packet from the current window
			} else if (sequenceNumber >= baseWindowPacketNumber) {
				//get packet index in current window
				int indexInWindow = sequenceNumber - baseWindowPacketNumber;
				//indicate package of that index received
				receivedInWindow[indexInWindow] = true;
				//record the data
				windowData[indexInWindow] = data;
				//confirm receiving packet
				sendAck(receivedPacket, sequenceNumber);

				/*System.out.printf("RECEIVED %d with #in window %d and base packet %d\n", sequenceNumber, numberInWindow,
				baseWindowPacketNumber);*/

				
				//if time to move the window
				if (sequenceNumber == baseWindowPacketNumber) {
					// write the data bytes to the output file

					//variable to hold the number of position to shift the window by
					int shift = 0;

					//find how many packets were also received in order (consecutive sequence numbers) after the 0th one in the current window
					for (int i = 0; i < windowSize; i++) {
						
						//if this was was received as well 
						if (receivedInWindow[i]) {
							//write the data
							output.write(windowData[i]);
							//increment the shift variable
							shift++;
							
							//if a packet was not received, then window can no longed be moved
						} else {
							//break the loop
							break;
						}
					}

					//shift the data left in the array to accommodate for their new indexes in arrays
					for (int i = 0; i < windowSize - shift; i++) {
						windowData[i] = windowData[i + shift];
						receivedInWindow[i] = receivedInWindow[i + shift];

					}
					
					//indicate that packets after the shifted ones were not received
					for (int i = windowSize - shift; i < windowSize; i++) {
						receivedInWindow[i] = false;
					}
					
					//increase the window base by the shift amount
					baseWindowPacketNumber += shift;


					if (header[0] == 1) {
						// close the socket
						datagramSocket.close();
						// close the file
						output.close();
						// exit the program
						System.exit(0);
					}

				}

			}

		}

	}

	private static void sendAck(DatagramPacket receivedPacket, int ackNumber) throws IOException {
		// send reply requesting the newly (or same) requested packet
		InetAddress senderIP = receivedPacket.getAddress();
		// get the port of the sender
		int senderPort = receivedPacket.getPort();

		// confirm having received that message
		byte[] requestedSequenceNumber = intToByteArray(ackNumber);
		// only take the LSBs as 16 bytes max sequence number
		byte[] packetRequest = { requestedSequenceNumber[2], requestedSequenceNumber[3] };
		// form the daragram packet to be sent
		DatagramPacket sendingPacket = new DatagramPacket(packetRequest, packetRequest.length, senderIP, senderPort);

		// send the packet only if there was a previously received packet (so when expecting the 0th packet, we let if time out)
		if (baseWindowPacketNumber - 1 >= 0) {
			datagramSocket.send(sendingPacket);
		}

	}

	// function that return the integer represented by the byte array
	private static int byteArrayToInt(byte[] bytes) {
		return java.nio.ByteBuffer.wrap(bytes).getInt();
	}

	// function that transfroms an integer into a byte array representing it
	private static byte[] intToByteArray(int integer) {
		return ByteBuffer.allocate(4).putInt(integer).array();
	}
}