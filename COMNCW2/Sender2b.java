
/* Jevgenij Zubovskij s1346981 */

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentLinkedQueue;

class Sender2b {

	// constants for sizes of the packet
	static final int payloadSize = 1024;
	static final int headerSize = 3;
	static final int packetSize = 1027;

	// command line arguments
	static String inputFileName;
	static int listeningPort;
	static String ipName;
	static int retryTimeout = 20;
	static int windowSize = 10;

	// time counter for the transmission time
	static int transmissionTime = 0;
	// start time
	static int startTime = 0;
	// total resending counter
	static int totalRetransissions = 0;
	// the throughput
	static double throughput = 0;

	// data to be sent
	static byte[] fileData;

	// array holding data on which packets are being / were sent to not be
	// repeatedly sent
	static boolean[] packetWasSent;

	// array holding data on which packets were received
	static boolean[] ackReceived;

	// packet count to be sent
	static int packetCount = 0;

	// maximum number of timeouts
	static int timeoutMax = 100;

	// making base packet number global
	static int basePacketNumber = 0;

	// array holding the timeout threads for packets
	static PacketTimeout[] timeouts;

	// the datagram socket
	static DatagramSocket datagramSocket;

	// thread holding the ack receiver
	static Thread receiver;
	// thread holding the packet sender
	static Thread sender;

	static volatile ConcurrentLinkedQueue<DatagramPacket> packetQueue;

	// class designed to run as the sending thread
	private static class SendTask implements Runnable {

		@Override
		public void run() {

			// while not all packets have been sent
			while (basePacketNumber < packetCount) {

				// send all the packets enqueued by the Timeout threads
				while (!packetQueue.isEmpty()) {
					try {
						datagramSocket.send(packetQueue.remove());
					} catch (IOException e1) {
						e1.printStackTrace();
					}

				}

			}

		}
	}

	// class designed to be a thread for sending and timing each individual
	// packet within the window size
	static class PacketTimeout {
		// timer and task to time out the system
		TimerTask timeoutTask;
		Timer timeoutTimer;
		// timeouts when sending the packet
		int timeoutsOnPacket = 0;
		// the packet number whose sending is assigned to this thread
		int packetNumber = 0;
		// indicator if thread is idle or not
		boolean free = true;
		
		DatagramPacket packet;

		// index of the thread in the Timeout array (for debugging purposes)
		int index;

		// constructor of the class
		public PacketTimeout(int index) {
			this.index = index;
			timeoutTimer = new Timer();
		}

		// method designated to stop the timeout
		public void stopTimeout() {

			// indicate thread idle
			free = true;
			// cancel the task
			timeoutTask.cancel();
			//purge the old task queue
			timeoutTimer.purge();
			// reset the timeout count for the packet
			timeoutsOnPacket = 0;
		}

		// method designated to restart the timeout time
		public void startTimeout(int newPacketNumber) throws IOException {

			// assign packet number to this thread
			packetNumber = newPacketNumber;
			// indicate thsi thread is no longer idle
			free = false;
			// renew the timer
			timeoutTimer = new Timer();
			// indicate assigned packet is being sent
			packetWasSent[packetNumber] = true;
			//get the packet to be sent by this timer
			packet = getNextPacket(packetNumber, packetCount, fileData);

			// TODO check which method of the two

			// send packet (as the timer counts retransmissions, and the initial sending is not a retransmission
			datagramSocket.send(packet);
			
			packetQueue.add(packet);

			// renew timer task
			timeoutTask = new TimerTask() {
				@Override
				public void run() {

					// send the packet again
					try {
						datagramSocket.send(packet);
					} catch (IOException e1) {
						e1.printStackTrace();
					}

					// TODO check which method of the two
					/*
					try {
						packetQueue.add(getNextPacket(packetNumber, packetCount, fileData));
					} catch (UnknownHostException e) {
						e.printStackTrace();
					}*/

					// record retransmission attempt globally and for current
					// packet
					totalRetransissions++;
					timeoutsOnPacket++;
					// if sending packet has timed out the maximum amount of
					// times, terminate Sender
					if (timeoutsOnPacket == timeoutMax) {
						basePacketNumber = packetCount;
					}

				}
			};
			// schedule the task to repeat with a period of retryTimeout, first
			// time after a delay of retryTimeout
			timeoutTimer.schedule(timeoutTask, retryTimeout, retryTimeout);

		}

	}

	// the class designed to run as the ack thread
	static class AckTask implements Runnable {

		@Override
		public void run() {
			// do this until all packets were sent (or terminated via same
			// condition)
			while (basePacketNumber < packetCount) {

				// set the holder for the ack
				DatagramPacket receivedPacket = getNextRecievalPacket();
				// timeout the waiting time for the reply after the set time
				try {

					// time out the connection if packets not being received and
					// thus terminate the ack thread
					datagramSocket.setSoTimeout(retryTimeout * timeoutMax);
					try {

						// try receiving the data
						datagramSocket.receive(receivedPacket);
						// then get the data
						byte[] receivedData = receivedPacket.getData();
						// get the sequence number the Receiver ack'ed
						int receivedRequestedSequenceNumber = byteArrayToInt(
								new byte[] { 0, 0, receivedData[0], receivedData[1] });

						// stop the timeout (sending) for the ack's sequence
						// number
						stopTimeouts(receivedRequestedSequenceNumber);
						// record the ack being received for the packet
						ackReceived[receivedRequestedSequenceNumber] = true;
						// if the ack was for the current window
						if (receivedRequestedSequenceNumber >= basePacketNumber) {
							// if the ack is for the base of the current window
							if (basePacketNumber == receivedRequestedSequenceNumber) {
								// then we can shift the window
								int currentbasePacketNumber = basePacketNumber;
								// shift the window as long as the acks received
								// were sequential (1, 2, , 4 etc.)
								for (int i = 0; i < windowSize && currentbasePacketNumber + i < packetCount; i++) {
									if (ackReceived[currentbasePacketNumber + i]) {
										basePacketNumber++;
									}
									// if there are no longer any acks received
									// in a line, break the loop
									else {
										break;
									}
								}

								// if there still is something to send
								if (basePacketNumber < packetCount) {
									// start sending the remaining packets
									for (int i = 0; i < windowSize && basePacketNumber + i < packetCount; i++) {
										// start the timers
										startTimeouts(basePacketNumber + i);
									}
								}
							}
						}
					} catch (SocketTimeoutException e) {
						// otherwise system should exit
					}

				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

	// method that makes the first encountered free timer send the packet with
	// the indicated number
	private static void startTimeouts(int packetNumber) throws IOException {

		//in case due to some increment the loop does nto cancel in time
		if (packetNumber < packetCount) {
			// if the packet was already sent / is being sent, do not assign to
			// a new thread
			if (packetWasSent[packetNumber]) {
				return;
			}

			// otherwise, find the closes free timer and assign it the packet
			// number to send
			for (int i = 0; i < windowSize; i++) {
				// make sure it is not null
				if (timeouts[i] != null) {
					if (timeouts[i].free) {
						timeouts[i].startTimeout(packetNumber);
						// break loop as packet assigned
						break;

					}
					// otherwise initialize first
				} else {
					timeouts[i] = new PacketTimeout(i);
					timeouts[i].startTimeout(packetNumber);
					// break loop as packet assigned
					break;
				}
			}
		}
	}

	// method that stops the non-idle timer (thread)(sending the packet with the
	// packet number passed in as the argument
	private static void stopTimeouts(int packetNumber) {
		for (int i = 0; i < windowSize; i++) {
			if (!(timeouts[i].free) && timeouts[i].packetNumber == packetNumber) {
				timeouts[i].stopTimeout();
				break;
			}
		}

	}

	public static void main(String args[]) throws Exception {

		// obtaing the ip name to send to
		ipName = args[0];
		// obtaing the sending port from the command line
		listeningPort = Integer.parseInt(args[1]);
		// obtain the file to send to from the command line
		inputFileName = args[2];

		// if we have the delay as the argument
		if (args.length > 3) {
			// obtain the timeout value from the command line
			retryTimeout = Integer.parseInt(args[3]);
		}

		// if we have the window size as the argument
		if (args.length > 4) {
			// obtain the window size value from the command line
			windowSize = Integer.parseInt(args[4]);
		}

		// creating the sending socket
		datagramSocket = new DatagramSocket();

		// get the file using the filename
		File file = new File(inputFileName);
		// read all the data from the file
		fileData = new byte[(int) file.length()];
		// open a file stream to read from the file
		DataInputStream dataInputStream = new DataInputStream(new FileInputStream(file));
		// read all the bytes from the file
		dataInputStream.readFully(fileData);
		// close data input srteam as all data read
		dataInputStream.close();

		// obtain payload (and hence packet) count to be sent
		packetCount = (int) Math.ceil(fileData.length / (payloadSize * 1.0f));

		// initialise arrays to hold ack and sending flags
		packetWasSent = new boolean[packetCount];
		ackReceived = new boolean[packetCount];

		// initialise the array values to false
		Arrays.fill(packetWasSent, Boolean.FALSE);
		Arrays.fill(ackReceived, Boolean.FALSE);

		// initialise the packet (to be sent out) queue
		packetQueue = new ConcurrentLinkedQueue<DatagramPacket>();

		// record the transmission start time
		startTime = (int) System.currentTimeMillis();

		// initalise the ack and sender threads
		sender = new Thread(new SendTask());
		receiver = new Thread(new AckTask());

		// TODO which method to use

		// start the threads
		receiver.start();
		// sender.start();

		// initialise the timeout
		timeouts = new PacketTimeout[windowSize];
		for (int i = 0; i < windowSize; i++) {
			/// make the timers
			timeouts[i] = new PacketTimeout(i);
			// start the timers
			timeouts[i].startTimeout(basePacketNumber + i);
		}

		// make sure main does not finish before the threads do

		// sender.join();

		receiver.join();
		// exit the system
		teminateSender();
	}

	// method designated to terminate the sender and print the statistics
	private static void teminateSender() {
		// end of transmission counter here as the remaining actions after the
		// last ACK is minimal
		transmissionTime = ((int) System.currentTimeMillis()) - startTime;
		// throughput calculations in KB/s
		throughput = (packetCount * 1000.0f) / (transmissionTime);
		// printing out the results
		System.out.printf(
				"-------------------------------------------\nThroughput: %.3f KB/s \nRetransmission count: %d\n-------------------------------------------\n",
				throughput, totalRetransissions);

		// when done sending close the socket and exit
		datagramSocket.close();

		// exit system
		System.exit(0);

	}

	// function that returns an empty datagram using the defined parameters
	private static DatagramPacket getNextRecievalPacket() {
		return new DatagramPacket(new byte[packetSize], packetSize);
	}

	public static DatagramPacket getNextPacket(int packetNumber, int packetCount, byte[] fileData)
			throws UnknownHostException {
		// prepare buffers for sending the data
		byte[] data;
		byte[] header = new byte[headerSize];
		byte[] packetContents;
		// get the next payload from the file
		data = Arrays.copyOfRange(fileData, packetNumber * payloadSize,
				((packetNumber + 1) * payloadSize >= fileData.length) ? fileData.length
						: (packetNumber + 1) * payloadSize);
		// set header 0th bit to 1 if this is the lays packet to be sent
		header[0] = (byte) (((packetNumber + 1 == packetCount) ? 1 : 0) & (0xff));
		// transfrom the packet numberinto a byte array

		byte sequenceNumber[] = intToByteArray(getNextPacketNumber(packetNumber));

		// using bytes 2 and 3 as we have byte order as such MSB .... LSB
		// and we sue the two LSBs
		header[1] = sequenceNumber[2];
		header[2] = sequenceNumber[3];
		// make the packet out of the header and the payload
		packetContents = concatenateByteArrays(header, data);

		// getting IP from the ip name
		InetAddress receiverIP = InetAddress.getByName(ipName);
		// prepare the datagram for sending
		DatagramPacket packet = new DatagramPacket(packetContents, packetContents.length, receiverIP, listeningPort);
		// send the datagram
		return packet;
	}

	private static int getNextPacketNumber(int packetNumber) {
		return packetNumber;
	}


	// funciton that concatenates two byte arrays int oone in the order they
	// are
	// in the arguments
	private static byte[] concatenateByteArrays(byte[] arrayOne, byte[] arrayTwo) {
		// creates the result array big enough to hodl both arguments
		byte[] result = new byte[arrayOne.length + arrayTwo.length];
		System.arraycopy(arrayOne, 0, result, 0, arrayOne.length);
		System.arraycopy(arrayTwo, 0, result, arrayOne.length, arrayTwo.length);
		// returns the concatenation result
		return result;
	}

	// function that transfroms an integer into a byte array representing it
	private static byte[] intToByteArray(int integer) {
		return ByteBuffer.allocate(4).putInt(integer).array();
	}

	// function that return the integer represented by the byte array
	private static int byteArrayToInt(byte[] bytes) {
		return java.nio.ByteBuffer.wrap(bytes).getInt();
	}

}